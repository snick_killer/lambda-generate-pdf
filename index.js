const chromium = require("chrome-aws-lambda");
const { join } = require("path");
const { readFileSync } = require("fs");

const Axios = require('axios');

const AWS = require("aws-sdk");
AWS.config.update({ region: "us-west-2" });
const s3 = new AWS.S3();

exports.handler = async (event) => {

    let browser = null;
    try {
        let body = JSON.parse(event['Records'][0].body)
        const puppeteer = chromium.puppeteer;

        browser = await puppeteer.launch({
            headless: true,
            args: chromium.args,
            defaultViewport: chromium.defaultViewport,
            executablePath: await chromium.executablePath,
        });

        const dirTemplate = join(`${__dirname}`, "template", "e-ticket.html");
        const html = readFileSync(dirTemplate, "utf-8");

        let messageBody = body.body;
        for(let i = 0; i < messageBody.length; i++) {

            // REMPLAZAR VALORES DEL TEMPLATE
            let parseTemplate = html.replace("%%nombre_evento%%", messageBody[i].nombre_evento);
            parseTemplate = parseTemplate.replace("%%qr%%", messageBody[i].qr);
            parseTemplate = parseTemplate.replace("%%img_evento%%", messageBody[i].imagenTicket);
            parseTemplate = parseTemplate.replace("%%nombre_apellido%%", messageBody[i].nombre_apellido);
            parseTemplate = parseTemplate.replace("%%dni%%", messageBody[i].dni);
            parseTemplate = parseTemplate.replace("%%entrada_color%%", messageBody[i].entrada_color);
            parseTemplate = parseTemplate.replace("%%nombre_entrada%%", messageBody[i].nombre_entrada);
            parseTemplate = parseTemplate.replace("%%fecha%%", messageBody[i].fecha);
            parseTemplate = parseTemplate.replace("%%horario%%", messageBody[i].horario);
            parseTemplate = parseTemplate.replace("%%numero_entrada%%", messageBody[i].numero_entrada);
            parseTemplate = parseTemplate.replace("%%id_compra%%", messageBody[i].id_compra);
            parseTemplate = parseTemplate.replace("%%precio_entrada%%", messageBody[i].precio_entrada);
            parseTemplate = parseTemplate.replace("%%organizador%%", messageBody[i].organizador);
            parseTemplate = parseTemplate.replace("%%ubicacion%%", messageBody[i].ubicacion);
            parseTemplate = parseTemplate.replace("%%direccion%%", messageBody[i].direccion);
    
            const page = await browser.newPage();
            await page.setContent(parseTemplate, {
                waitUntil: "load",
            });
            const buffer = await page.pdf({
                format: "A4",
                landscape: false,
            });
    
            let output_filename = messageBody[i].namepdf;
    
            let r = await upload_pdf_s3(output_filename, buffer);
        }

        // send confirmacion to accessgo
        await sendRequest(body);

        return;
    } catch (error) {
        console.error(error);
    } finally {
        browser.close();
    }
};

function upload_pdf_s3(output_filename, buffer) {
    return new Promise((resolve, reject) => {

        // UPLOAD S3
        const s3Params = {
            ACL: 'public-read',
            Body: buffer,
            ContentType: "application/pdf",
            Bucket: "accessgo",
            Key: `pdf/${output_filename}`,
        };

        s3.putObject(s3Params,async  (err, result) => {
            if(err) resolve(err);
            else {
                resolve(result);
            }
        })
    })
}

async function sendRequest(body) {
    try {
        const instance = Axios.create({
            baseURL: 'https://accessgo.com.ar/',
            // baseURL: 'https://c76f-45-175-103-84.sa.ngrok.io/',
            timeout: 5000,
            headers: {
                'Content-Type': 'application/json',
            }
        });
    
        let response = await instance.post('api/confirmacion/pdf', {body, hash: 'b7f56c82d2b42ba9800c231031cda856'})
        if( response.data.r.c == 1 ) {
            console.log(response.data.r.m)
        }
        return;
    } catch (error) {
        console.log(error);
        return;
    }
}
